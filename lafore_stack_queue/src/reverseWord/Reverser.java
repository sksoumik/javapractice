package reverseWord;

public class Reverser {
	
	private String input;
	private String output;
	
	public Reverser(String i )
	{
		input = i;
	}
	
	//------------------------------------------------------
	
	public String doRev(){
		int stacksize = input.length();
		StackX x = new StackX(stacksize);
		
		for(int i=0 ; i< input.length();i++)
		{
			char ch = input.charAt(i);
		    x.push(ch);	
		}
		output = "";
		while(! x.isEmpty())
		{
			char ch = x.pop();
			output = output+ ch ;
		}
		return output;
		
	}
	

}
