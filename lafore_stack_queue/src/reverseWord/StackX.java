package reverseWord;

public class StackX {
	private int maxsize;
	private char [] stackArray;
	private int top;
	
	public StackX(int max)
	{
		maxsize = max;
		stackArray = new char [maxsize];
		top = -1;
		
	}
	
	public void push (char j)
	{
		stackArray[++top]= j;
	}
	
	public char pop()  // removing the top item
	{
		return stackArray[top--];
	}
	
	public char peek()   // returning the top item of the stack 
	{
		return stackArray[top];
	}
	
	public boolean isEmpty()
	{
		return (top==-1);
		
	}
	
	public boolean isFull()
	{
		return (top==maxsize-1);
		
	}

}
