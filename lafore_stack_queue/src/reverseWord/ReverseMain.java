package reverseWord;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ReverseMain {

	public static void main(String[] args) throws IOException {
		
		String input, output;
		while(true)
		{
			System.out.println("Enter a String: ");
			System.out.flush();
			
			input = getString();
			if(input.equals(""))
				break;
			
			Reverser r = new Reverser(input);
			output = r.doRev();
			System.out.println("reversed: "+output);
		}

	}
	
	//-------------------------
	
	public static String getString() throws IOException
	{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		String  s =br.readLine();
		return s;
	}

}
