package stack01;

public class StackX {
	private int maxsize;
	private int [] stackArray;
	private int top;
	
	public StackX(int s) {
		maxsize = s;
		stackArray = new int[maxsize];
		top = -1;
	}

	public void push(int j) {
		stackArray[++top] = j;
	}

	public long pop() // removing the top item
	{
		return stackArray[top--];
	}

	public long peek() // returning the top item of the stack
	{
		return stackArray[top];
	}

	public boolean isEmpty() {
		return (top == -1);

	}

	public boolean isFull() {
		return (top == maxsize - 1);

	}
	public void print(StackX s)
	{
	 for(int i =top; i>=0;i--)
	   System.out.print(stackArray[i]+" "); 
	}
	/*
	 * public void display() { for(int i=0;i<stackArray.length;i++) {
	 * System.out.print(stackArray[i]+" "); } }
	 */

}
