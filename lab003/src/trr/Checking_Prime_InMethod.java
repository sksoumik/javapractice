package trr;

import java.util.Scanner;

public class Checking_Prime_InMethod {
	public static  boolean isPrime(int number){
		for(int div=2;div<=number/2;div++){
			if(number%div==0){
				return false;
				
			}
		}
		return true;
		
	}

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Enter the number to check: ");
		int num=input.nextInt();
		System.out.println(num+ " :: "+isPrime(num));
		
		
		

	}

}
