package trr;

import java.util.Scanner;

public class SumOfArray_raw_column_diagonal {

	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);
		int sum_raw=0;
		int sum_col=0;
		int sum_diagonal=0;
		//int sum_diagonal2=0;

		int[][] num=new int [3][3];
		System.out.println("enter the array elements: ");
		
		for (int raw=0;raw<num.length;raw++){
			for(int col=0;col<num.length;col++){
				num[raw][col]=input.nextInt();
				
			}
		}
		System.out.println("the matrix is like: ");
		for (int raw=0;raw<num.length;raw++){
			for(int col=0;col<num.length;col++){
				System.out.print(num[raw][col]+"  ");
				
			}
			System.out.println();
		}
		System.out.println("Sum of the raw: ");
		for (int raw=0;raw<num.length;raw++){
			for(int col=0;col<num.length;col++){
				sum_raw+=num[raw][col];
				
			}
		}
		System.out.println(sum_raw);
		
		System.out.println("Sum of the col: ");
		for (int raw=0;raw<num.length;raw++){
			for(int col=0;col<num.length;col++){
				sum_col+=num[col][raw];
				
			}
		}
		System.out.println(sum_col);
		
		System.out.println("Sum of the diagonal 1 is : ");
		for (int raw=0;raw<num.length;raw++){
			for(int col=0;col<num[raw].length;col++){
				sum_diagonal+=num[raw][raw];
				
			}
		}
		System.out.println(sum_diagonal);
		
		/*System.out.println("Sum of the diagonal  is : ");
		for (int raw=0;raw<num.length;raw++){
			for(int col=0;col<num[raw].length-1;col--){
				sum_diagonal1+=num[raw][raw];
				
			}
		}
		System.out.println(sum_diagonal1);*/ 
		}
	

	}


