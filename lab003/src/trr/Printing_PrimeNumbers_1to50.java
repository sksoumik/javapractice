package trr;

public class Printing_PrimeNumbers_1to50 {

	public static void main(String[] args) {
		System.out.println("The first 50 prime numbers are: ");
		printPrimeNumbers(50  );


	}
	public static void printPrimeNumbers(int n_Primes){
		int perLine=10;
		int count=0;
		int num=2;
		
		while(count<n_Primes){
			if(isPrime(num)){
				count++;
				if(count%perLine==0){
					System.out.printf("%-5s\n",num);
				}
				else
					System.out.printf("%-5s",num);
			}
			num++;
			
		}
	}
	public static boolean isPrime(int num){
		for(int div=2;div<=num/2;div++){
			if(num%div==0){
				return false;
			}
		}
		return true;
	}

}
