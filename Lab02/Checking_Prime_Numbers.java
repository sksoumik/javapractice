package labproblems;

import java.util.Scanner;

public class Checking_Prime_Numbers {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println(" Enter a number to check : ");
		int num=input.nextInt();
		boolean isPrime=true;
		
		for( int i=2; i<num/2;i++){
			if(num%2==0)
		  isPrime=false;
		}
		if(isPrime==true)
			System.out.println("this is a prime number");
		else
			System.out.println("this is not a prime number");

	}

}
