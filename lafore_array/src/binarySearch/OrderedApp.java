package binarySearch;

public class OrderedApp {

	public static void main(String[] args) {
		int maxsize=100;
		
		OrdArray arr;
		 arr=new OrdArray(maxsize);
		 
		 arr.insert(77);
		 arr.insert(44);
		 arr.insert(12);
		 arr.insert(99);
		 arr.insert(4);
		 arr.insert(23);
		 arr.insert(19);
		 System.out.println("The items are: ");
		 arr.display();
		 
		 
		 int searchkey = 4;
		 System.out.println("\nThe array size is "+arr.size());
		 
		 if(arr.find(searchkey) != arr.size())
			 System.out.println("Found "+searchkey);
		 else
			 System.out.println("Can't find "+searchkey);
		 
		 arr.delete(77);
		 arr.delete(12);
		 arr.delete(23);
		 System.out.println("\nAfter deleting three elements from the array: ");
		 arr.display();
		 System.out.println("\nThe array size is "+arr.size());

	}

}
