package binarySearch;

public class OrdArray {
	
	private long [] a;
	private int n;  // number of data items
	
	//***********Constructor****************
	public OrdArray(int max)
	{
		a = new long[max];  //creating an array;
		n = 0;
	}
	
	/**************Size of the array*********/
	public int size()
	{
		return n;
	}
	
	//***********Binary search method ************* 
	public int find(long searchKey)
	{
		int lowerBound = 0;
		int UpperBound = n-1;
		int CurrentIndex;
		
		while(true)
		{
			CurrentIndex = (lowerBound + UpperBound)/2;
			if(a[CurrentIndex]==searchKey)
			{
				return CurrentIndex; //item found 
			}
			else if (lowerBound > UpperBound)
				return n; // can't find it;
			else
			{
				if(a[CurrentIndex]<searchKey)
					lowerBound = CurrentIndex + 1;  // it's in the upper half
				else
					UpperBound = CurrentIndex - 1; // it's in the lower half 
			}
		}
		
	}

	//*********put element into array***********
	
	
	public void insert(long value)
	{
		int i;
		for( i = 0; i < n; i++)
		{
			if(a[i] > value)
				break;
		}
		
		 for(int k = n; k>i; k--)
		 {
			 a[k]=a[k-1]; // move bigger ones up
			 
		 }
		 a[i]=  value;
		 n++;
		 
}
	public void display()
	{
		for(int m=0; m<n;m++)
		{
			System.out.print(a[m]+" ");
			
		}
	}
	public boolean delete(long value)
	{
		int j = find(value);
		
		if(j == n)
			return false;
		else
		{
			for(int k=j; k<n; k++)
			{
				a[k]=a[k+1];
			}
			n--;
			return true;

		
				
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
