package nameAndAgeFind;



public class MainMethod {

	public static void main(String[] args) {
		DataArray d;
		int max = 100;
		
		d = new DataArray(max);
		
		d.insert("Soumik", "Sadman", 22);
		d.insert("Nadia", "Julie", 22);
		d.insert("Sourav", "Faysal", 22);
		d.insert("Tanisha", "Oroni", 22);
		d.displaA();
		System.out.println("\n********************************");
		
		Person found;
		String searchName = "Julie";      // it will show they can't find julie because it's the first name ...... 
		
		found = d.find(searchName);
		
		if(found != null)
		{
			System.out.print("\nFound"); 
		    found.displayPerson();

		}
		else
			System.out.println("Can't find "+searchName);   
		// I have to search with the last name ..... 
     String searchName2 = "Nadia";
     System.out.println("\n************************************");
		
		found = d.find(searchName2);
		
		if(found != null)
		{
			System.out.print("Found");
		    found.displayPerson();

		}
		else
			System.out.println("Can't find "+searchName2);
		
		 System.out.println("\n************************************");
		
		d.delete("Nadia");
		System.out.println("\nAfter Deleting Nadia the list is now: ");
		d.displaA();
		
		
		
		
	}

}
