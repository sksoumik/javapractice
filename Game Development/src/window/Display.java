package window;

import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Display {
	private JFrame frame;
	private Canvas canvas;
	private String title;
	private int height,width;
	
	public Display(String title, int width, int height) {
	
		this.title = title;
		this.width = width;
		this.height = height;
		creatDisplay();
	}

	public void creatDisplay(){
		frame=new JFrame(title);
		frame.setSize(width, height);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setResizable(true);
		frame.setLocationRelativeTo(null);
		
		canvas=new Canvas();
		canvas.setPreferredSize(new Dimension(height,width));
		canvas.setMaximumSize(new Dimension(height,width));
		canvas.setMinimumSize(new Dimension(height,width));
		frame.add(canvas);
		frame.pack();

		
		
		
		

		
	}

}
