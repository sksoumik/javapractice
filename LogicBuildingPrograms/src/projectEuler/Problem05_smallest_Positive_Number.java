package projectEuler;

public class Problem05_smallest_Positive_Number {
	
	 static int GCD(int a, int b)
		{
			if(a%b==0)
				return b;
			else
				return GCD(b, a%b);                                          /*a  = q   b    r
				                                                               18 = 2 * 8    2
				                                                               8  = 4 * 2    0 
				                                                               when the reminder is zero return b and b is the GCD*/
			
		}
		
	                                                                          /*     LCM formula is  (a*b)/GCD(a,b)         */
	 
	static int lcm(int a, int b)
	{
	    return (a*b)/GCD(a,b);
	}

	public static void main(String[] args) { 
		int result = 1;
		
		for(int i=2; i<20; i++)
		{
			result = lcm(result,i);
		}
		System.out.println(result);
		
	
		
		

	}

}
