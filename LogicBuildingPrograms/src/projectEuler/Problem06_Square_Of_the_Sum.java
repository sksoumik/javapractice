package projectEuler;

 /* The sum of the squares of the first ten natural numbers is,

1^2 + 2^2 + ... + 10^2 = 385
The square of the sum of the first ten natural numbers is,

(1 + 2 + ... + 10)2 = 552 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.*/

public class Problem06_Square_Of_the_Sum {
	
	
		
	static int SumSquare()     //1^2 + 2^2 + ... + 10^2 = 385
		{
			int i=0; int sum = 0 ;
			for(i=1;i<=100;i++)
			{
				sum = sum+(i*i);
			}
			return sum;
		}
	
	static int SquareSum()   // 1 + 2 + ... + 10)^2 = 55^2 = 3025
	{
		int sum =0; 
		
		for(int j=1;j<=100;j++)
		{
			sum =  sum+j;
			
		}
		return sum*sum;
	}
	public static void main(String[] args) {
		int difference = SquareSum()- SumSquare();
		System.out.println(difference);
		
	}
	
	
	

}
