package projectEuler;


//The prime factors of 13195 are 5, 7, 13 and 29.

//What is the largest prime factor of the number 600851475143 ?

public class Problem03_LargestPrimeFactor {

	public static void main(String[] args) {
	    long num =  600851475143l;        // in case of using long we must have to give a "L" after the number; 
		for(int div =2; div <= num ;div ++)
		{
			while(num % div == 0){
				
			
				System.out.print(div +" ");
			
			num = num/ div;
			}
		}

	}

}
