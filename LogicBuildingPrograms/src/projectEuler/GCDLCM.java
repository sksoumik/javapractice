package projectEuler;

import java.util.Scanner;

// Euler Project 05
public class GCDLCM {
	
	 static int GCD(int a, int b)
	{
		if(a%b==0)
			return b;
		else
			return GCD(b, a%b);                                          /*a  = q   b    r
			                                                               18 = 2 * 8    2
			                                                               8  = 4 * 2    0 
			                                                               when the reminder is zero return b and b is the GCD*/
		
	}
	
                                                                          /*     LCM formula is  (a*b)/GCD(a,b)         */
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Enter the value of x and y: ");
		int x = input.nextInt();
		int y = input.nextInt();
		System.out.println("GCD is :"+GCD(x,y));
		int LCM = (x*y)/GCD(x,y);
		System.out.println("LCM is :"+LCM);
		
		

	}

}
