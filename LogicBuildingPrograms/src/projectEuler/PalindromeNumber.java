package projectEuler;

// Problem04

//A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 � 99.

//Find the largest palindrome made from the product of two 3-digit numbers.// 


public class PalindromeNumber {
	
	public static boolean isPalindrome(String s)
	{
		if(s.length()<=1)
			return true;
		else if(s.charAt(0)!=s.charAt(s.length()-1))
			return false;
		else
			return isPalindrome(s.substring(1, s.length()-1));
		
	}

	public static void main(String[] args) {
		
		int max = 100001;
		for(int i = 999; i >= 100;i--)
		{
			if(max >= i*999)
				break;
			
			for(int j = 999; j>= i; j--)
			{
				int p = i*j;
				if(max<=p && isPalindrome(Integer.toString(p)))
				{
					max =p;
				}
			}
			
		}
		System.out.println(max);
		
		

	}

}
