package basics;

public class RandomNumber {

	public static void main(String[] args) {
		System.out.println(Math.random()); // returns 0 to 1 
		System.out.println(Math.random()*50); // returns 0 to 50
		System.out.println(Math.floor(Math.random()*100));

	}

}
