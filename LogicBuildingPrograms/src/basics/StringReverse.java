package basics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class StringReverse {

	public static void main(String[] args) {
		String input = "";
		System.out.println("Enter a string: ");
		try{
		
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		input = br.readLine();
		
		char [] ar = input.toCharArray();
		
		for(int i = input.length()-1; i>=0; i--)
		{
			System.out.print(ar[i]);
		}
		}catch(IOException e)
		{
			e.printStackTrace();
		}

	}

	
}
