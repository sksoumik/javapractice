package logicDevelopment;

public class TypecastingTest {
// typecasting is converting from one type to another data type
	// i can store a small value into a big value like byte to int
	// but i cant 
	public static void main(String[] args) {
		double big=127;       /* if its 128 still it will show a garbage cause a byte can only take value
		 upto 127 */ 
		int small=(int)big;
		System.out.println(small);

	}

}
