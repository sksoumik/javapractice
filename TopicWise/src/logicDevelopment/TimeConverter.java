package logicDevelopment;

import java.util.Scanner;

public class TimeConverter {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Enter speed (km/hr):  ");
		double speed=input.nextDouble();
		System.out.println("Enter distance in km: ");
		double distance=input.nextDouble();
		
		distance=distance*1000;  //in meter
		speed=speed*1000/3600;  //in second
		double time=distance/speed;
		System.out.println("the time is in second is:  "+time+ " seconds");  
		
		int hours=(int)time/3600;
		System.out.println(hours+" hours");
		
		int mins=(int)(time-hours*3600)/60;
		System.out.println(mins+" minutes"); 
		
		int seconds= (int)(time-hours*3600-mins*60);
		System.out.println(seconds+" seconds");
		

	}

}
