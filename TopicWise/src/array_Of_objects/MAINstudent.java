package array_Of_objects;

public class MAINstudent {

	public static void main(String[] args) {
		//Student soumik=new Student();
		//Student sadman=new Student("sadman", 21);
		//Student zara=new Student();
		
		
		Student[] students=new Student[3];
		
	    students[0]=new Student("soumik",21);
		students[1]=new Student("sadman",22);
		students[2]=new Student("zara",17);
		
		//soumik.setNameAndAge("soumik", 23);
		students[2].setNameAndAge("maria", 16);
		students[1].setNameAndAge("shehrin", 16);

		
		students[0].intoduce();
		students[1].intoduce();
		students[2].intoduce();
	}

}
