package abstract_classes;

public class Circle extends GeometricObject {
	
	private double radius;
	
	public Circle(){
	
		}
	
	public Circle(double radius){
		this.radius=radius;
		
	}
	public Circle(double radius, String color, boolean filled){
		this.radius=radius;
		setColor(color);
		setFilled(filled);
		
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}
	public double getDiameter(){
	
		return 2*radius;
		
	}
	public double getArea(){
		return 2*radius*radius*Math.PI;
	}
	public double getPerimeter(){
		return 2*Math.PI*radius;
	}
	//print the circle info
	public void printCircleInfo(){
		System.out.println(" The circle is created at "+getDateCreated()+ " and the radius is: "+radius);
	}
	
	
	

}
