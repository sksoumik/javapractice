package gui_basics;
import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Basic extends JFrame {
	
	private JLabel item1;
	
	public Basic(){
		super("The title of the window");
		setLayout(new FlowLayout());
		item1=new JLabel("This is a sentense");
		item1.setToolTipText("This will show up on the hover");
		add(item1);
		
	}

}
