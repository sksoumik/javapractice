package file_create_read;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class File_Reading {
	public static void main(String[]args) throws IOException{
		
		BufferedReader br=null;
		
		try {
			br= new BufferedReader(new FileReader("E:\\WorkplaceJava\\TopicWise\\sourav.txt"));
			String line;
			
			while((line=br.readLine())!=null){
				System.out.println(line);
			}
			
			
			
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		}finally{
			
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		
	}

}
