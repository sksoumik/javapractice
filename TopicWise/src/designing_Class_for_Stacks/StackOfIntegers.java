package designing_Class_for_Stacks;

public class StackOfIntegers {
	private int []elements;
	private int size;
	public static final int  DEFALUT_CAPACITY=16;
	
	public StackOfIntegers(){
		this (DEFALUT_CAPACITY);
		
	}
	public StackOfIntegers(int capacity){
		
		elements=new int[capacity];
		
	}
	public boolean empty(){
		return size==0;
	}
	public void push(int value){
		if(size>elements.length){
			int []temp=new int [elements.length*2];
			System.arraycopy(elements, 0, temp, 0, elements.length);
		}
		elements[size++]=value;
		
	}
	public int pop(){
		return elements[--size];
	}
	public int peak(){
		return elements[size-1];
	}
	public int getSize(){
		return size;
	}
	public static void main(String[]args){
		StackOfIntegers stack=new StackOfIntegers();
		for(int i=0;i<10;i++)
			stack.push(i);
		while(!stack.empty())
			System.out.println(stack.pop()+" ");
		
		
		
	}
			

	
}
