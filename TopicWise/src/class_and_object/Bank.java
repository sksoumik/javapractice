package class_and_object;

public class Bank {
	
	Account[] accounts;
	
	public Bank(int size){
		accounts=new Account[size];
	}
	
	public void addAccount(Account account, int index){
		if(index<accounts.length){
			accounts[index]=account;
		}
		
	}
	public Account getAccount(String number){
		
		Account a=null;
		
		for(int i=0;i<accounts.length;i++){
			if(accounts[i]!=null && accounts[i].getNumber().equals(number)){
				a = accounts[i];
			}
		}
		return a;
	}
	

}
