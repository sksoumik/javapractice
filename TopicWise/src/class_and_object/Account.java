package class_and_object;

public class Account {
	private String number;
	private String name;
	private double balance;
	public Account(String number, String name, double balance) {
		
		this.number = number;
		this.name = name;
		this.balance = balance;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	@Override
	public String toString() {
		return "Account [number=" + number + ", name=" + name + ", balance="
				+ balance + "]";
	}
	
	
	
	

	

}
