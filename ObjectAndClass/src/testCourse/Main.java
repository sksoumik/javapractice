package testCourse;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Enter the value of a,b and c: ");
		double a=input.nextDouble();
		double b=input.nextDouble();
		double c=input.nextDouble();

		QuadraticEquation root=new QuadraticEquation(a,b,c);
		System.out.println("root1 is ="+root.getRoot1()+"\n"+"root2 is="+root.getRoot2());

	}

}
