package testCourse;

public class QuadraticEquation {
	private double a,b,c;
	public QuadraticEquation(double a, double b, double c){
		this.a=a;
		this.b=b;
		this.c=c;
		
	}
	public double getA(){
		return a;
	}
	public double getB(){
		return b;
	}
	public double getC(){
		return c;
	}
	public double getDiscriminant(){
	    
		return b*b-4*a*c;
	}
	public double getRoot1(){
		double d=b*b-4*a*c;
		double r1=-b+Math.sqrt(d)/(2*a);
		
		if(d>=0)
			return r1;
		
		else
			return 0;
		
		
	}
	public double getRoot2(){
		double d=b*b-4*a*c;
		double r2=-b-Math.sqrt(d)/(2*a); 
		if(d>=0)
			return r2;
		else
			return 0;
	}
	
	

	 
	

	
	}


