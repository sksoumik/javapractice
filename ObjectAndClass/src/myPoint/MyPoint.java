package myPoint;

public class MyPoint {
	
	private double x; 
	private double y;
	
	public MyPoint(int x, int y){
		this.x=x;
		this.y=y;
		
	}
	public double getX(){
		return x;
	}
	public double getY(){
		return y;
	}
	public void setX(double x){  
		this.x=x;
	}
	public void setY(double y){ 
		this.y=y;
	}
	public MyPoint(int x1,int x2,int y1,int y2){
		x=x2-x1; 
		y=y2-y1;
		
	}
	public double distance(){
		double sum1=Math.pow(x, 2);
		double sum2=Math.pow(y, 2);
		double sum=sum1+sum2;
		double dis= Math.sqrt(sum); 
		return dis; 
		
	}

}
