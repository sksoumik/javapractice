package enumeration;
import java.util.EnumSet;


public class MainGirlfriend {
	
	public static void main(String[]args){
	
	for(Girlfriend info: Girlfriend.values())
		System.out.printf("%s%s%s\n",info,info.getDesc(),info.getTheirAge());
	
	
	
	/* for specific range, where to start and where to end*/   
	/* we need to first write the >import java.util.EnumSet;< statement first after package  */
	
	System.out.println("\n\nAnd now for the range of constants: \n");
	
	for(Girlfriend info:EnumSet.range(Girlfriend.shehrin, Girlfriend.adhoraa))
		System.out.printf("%s%s%s\n",info,info.getDesc(),info.getTheirAge());
 
		
		
	
	

	}
		}
	
	
	
	

	
	


