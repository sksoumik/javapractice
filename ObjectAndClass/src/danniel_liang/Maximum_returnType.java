package danniel_liang;

import java.util.Scanner;

public class Maximum_returnType {
	
	public static int max(int num1, int num2){
		
		
		int result;
		if(num1>num2)
			result=num1;
		else
			result=num2;
		return result;
		
	}

	public static void main(String[] args) {
		
		
		
		Scanner input=new Scanner(System.in);
		System.out.println("Enter the value of two numbers: ");
		int n1=input.nextInt();
		int n2=input.nextInt();
		
		
		System.out.println("The maximum number is: "+max( n1, n2));
		


	}

}
