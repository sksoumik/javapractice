package danniel_liang;

import java.util.Scanner;

public class GradePrinting_returnType {
	
	public static char getGrade(double score){
		
		if(score>=90)
			return 'A';
		else if(score>=80)
			return 'B';
		else if(score>=70)
			return 'C';
		else
			return 'F';
		
	}

	public static void main(String[] args) {
		
		
		Scanner input=new Scanner(System.in);
		System.out.println("Enter your mark: ");
		
		double mark=input.nextDouble();
		System.out.println("Your grade is: "+getGrade(mark));
		
		
		

	}

}
