package danniel_liang;

import java.util.Scanner;

public class Returning_Value {
	
	public static int sign(int num){
		if(num>0)
			return 1;
		else if(num==0)
			return 0;
		else
			return -1;
		
		
	}

	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);
		System.out.println("Enter value of the number: ");
		int n=input.nextInt();
		System.out.println(sign(n));

	}

}
