package experiment;

public class MyBirthday {
	
	private String name;
	private Exp birthday;
	
	public MyBirthday(String theName, Exp date){
		
		this.name=theName;
		this.birthday=date;
		
		
		
	}
	public String toString(){
		
		return String.format("My name is %s and my birthday is at %s", name, birthday);
	}
	

}
