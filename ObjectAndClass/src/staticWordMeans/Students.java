package staticWordMeans;

public class Students {
	
	
	int age;
	String name;
	int id;
	
	static int NoOfStdents=0;
	
	   Students(){
		   NoOfStdents++;
	   }
	
	public static int getNoOfStdents(){
		return NoOfStdents;
	}
	   
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	

}
