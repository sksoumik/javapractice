package method;

public class PalindromeNumber {
	
	
	public static int reverse(int num)
	{
		int reminder=0;
		while(num>0)
		{
			reminder=(num%10)+(reminder*10);
			num=num/10;
		}
		return reminder;
		
	}
	
	public static boolean isPalindrome(int num)
	{
		if(num==reverse(num))
			return true;
		else 
			return false;
	}

	public static void main(String[] args) {
		System.out.println(isPalindrome(1221));

	}

}
