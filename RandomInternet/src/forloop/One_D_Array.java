package forloop;

import java.util.Scanner;

public class One_D_Array {

	public static int GCD(int n1, int n2)
	{
		
		if(n2==0){
			return n1;
		}
		else
			return GCD(n2, n1%n2);
	}
	
	
	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);
		System.out.println("Enter two number::: ");
		int num1=input.nextInt();
		int num2=input.nextInt();
		
		System.out.println(GCD(num1,num2));
		

	}

}
