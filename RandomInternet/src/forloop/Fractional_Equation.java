package forloop;

/* write a program that sum the following equation : 
 * 
 * (100/1) + (99/2)  + (98/3)  +.......+ (3/98)+ (2/99) + (1/100) 
 * 
 * */


public class Fractional_Equation {

	public static void main(String[] args) {      
		
		int sum=0;
		int count=1;
		
		for(int i=100;i>=1;i--){
			sum+=i/count;
			count++;
		}
		System.out.println(sum);


	}

}
