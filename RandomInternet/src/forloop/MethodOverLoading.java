package forloop;

public class MethodOverLoading {

	public static void main(String[] args) {
		
		System.out.println(ADD(2,6));
		System.out.println(ADD(2.56,6.67));
		System.out.println(ADD("Sadman ","Soumik"));


		

	}
	
	public static int ADD(int a, int b){
		return (a+b);
	}
	public static double ADD(double a, double b){
		return (a+b);
	}
	public static String ADD(String a, String b){
		return (a+b);
	}

}
