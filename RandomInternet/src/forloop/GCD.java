package forloop;

import java.util.Scanner;

public class GCD {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Enter two integer value: ");
		int num1=input.nextInt();
		int num2=input.nextInt();
		System.out.println("The gcd of the "+GCD(num1,num2));

		
		

	}
	
	private static  int GCD(int a,int b){
		
		
		int t;
		while(b!=0){
			
			t=b;
			b=a%b;
			a=t;
			
		}
		return a;
	}

}
