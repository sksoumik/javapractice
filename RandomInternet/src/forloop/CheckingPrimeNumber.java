package forloop;

import java.util.Scanner;

public class CheckingPrimeNumber {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Enter a number to check : ");
		int num=input.nextInt();
		boolean isPrime = true;
		
		for(int i=2; i < num/2; i++) {
		if((num % i) == 0) {
		isPrime = false;
		break;
		}
		}
		if(isPrime) System.out.println("Prime");
		else System.out.println("Not Prime");
		} 
	}


