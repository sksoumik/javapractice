package forloop;

import java.util.Scanner;

public class Factorla {

	public static void main(String[] args) {
		
		System.out.println("Enter the number: ");
		Scanner input=new Scanner(System.in);
		int num=input.nextInt();
		int x=num;
		int fact=1;
		
		if(x==0 || x==1)System.out.println("1");
		else{
			while(x>1){
				fact=fact*x;
				x--;
			}
		}
		System.out.println("The value of the factorial of "+ num+ " is "+fact);

	}

}
