package loopTest;

import java.util.Scanner;

public class FindingMaximumNumber {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int number;
		int maximum=0;
		number=input.nextInt();
		while(number!=0)
		{
			number=input.nextInt();
			if(number> maximum)
			{
				maximum=number;
			}
		}
		System.out.println("max is "+maximum);
		System.out.println("number is "+number);

	}

}
