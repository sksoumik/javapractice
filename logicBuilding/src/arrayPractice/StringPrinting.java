package arrayPractice;

public class StringPrinting {
	
	public static void main(String[]args){
		
		
		String[][] salutation={
				
				{"Mr.", "Mrs.", "Ms."},
				{"Kumar"}
		};
		
		//Mr. Kumar
		System.out.println(salutation[0][0]+salutation[1][0]);
		//Mrs. kumar
		System.out.println(salutation[0][1]+salutation[1][0]);
	}

}
