package arrayPractice;

 class Findin_minimum_maximum {
	 
	 
	 public int min(int[][] matrix) {
	        int min = matrix[0][0];
	        for (int row = 0; row < matrix.length; row++) {
	            for (int col = 0; col < matrix[row].length; col++) { 
	                if (min > matrix[row][col]) {
	                    min = matrix[row][col];
	                }
	            }
	        }
	        return min;
	 }
	 public int max(int[][] matrix) {
	        int max = matrix[0][0];
	        for (int row = 0; row < matrix.length; row++) {
	            for (int col = 0; col < matrix[row].length; col++) {
	                if (max < matrix[row][col]) {
	                    max = matrix[row][col];
	                }
	            }
	        }
	        return max;
	    }

	public static void main(String[] args) {
		int[][] matrix = {{59, 4, 33}, {11, 13, 85}, {5, 25, 30}, {7, 1, 27}};
		Findin_minimum_maximum maxmin = new Findin_minimum_maximum();
        System.out.println("min value is: " + maxmin.min(matrix));
        System.out.println("max value is: " + maxmin.max(matrix));

	}

}
