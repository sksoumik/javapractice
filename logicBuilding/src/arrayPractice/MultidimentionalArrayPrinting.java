package arrayPractice;
class MinMaxInMatrix {
	 
    public int min(int[][] matrix) {
        int min = matrix[0][0];
        for (int col = 0; col < matrix.length; col++) {
            for (int row = 0; row < matrix[col].length; row++) { 
                if (min > matrix[col][row]) {
                    min = matrix[col][row];
                }
            }
        }
        return min;
    }
 
    public int max(int[][] matrix) {
        int max = matrix[0][0];
        for (int col = 0; col < matrix.length; col++) {
            for (int row = 0; row < matrix[col].length; row++) {
                if (max < matrix[col][row]) {
                    max = matrix[col][row];
                }
            }
        }
        return max;
    }
 
    public static void main(String[] args) {
        int[][] matrix = {{59, 4, 33}, {11, 13, 85}, {5, 25, 30}, {7, 1, 27}};
        MinMaxInMatrix maxmin = new MinMaxInMatrix();
        System.out.println("min value is: " + maxmin.min(matrix));
        System.out.println("max value is: " + maxmin.max(matrix));
    }
}