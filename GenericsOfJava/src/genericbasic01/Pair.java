package genericbasic01;

public interface Pair<K,V> {
	
	public K getKey();
	public V getValue();
	

}
