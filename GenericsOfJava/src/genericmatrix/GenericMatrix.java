package genericmatrix;



public abstract class GenericMatrix<E extends Number> {
	
	// for adding two elements in the matrix
	protected abstract E add(E o1, E o2);
	
	//for multiplying two elements of the matrix
	
	protected abstract E multiply(E o1, E o2);
	
	//abstract method for the defining zero for the matrix element.
	
	protected abstract E zero();
	
	// add two matrices
	
	public E[][] addMatrix(E[][] matrix1, E[][]matrix2)
	{
		// check  bounds of the two matrix 
		
		if((matrix1.length!=matrix2.length)||(matrix1[0].length!=matrix2[0].length))
		{
			throw new RuntimeException("The matrices do not have the same size");
		}
		E[][]result =(E[][])new Number[matrix1.length][matrix1[0].length];
		
		//perform addition
		for(int i=0;i<result.length;i++)
		{
			for(int j=0;j<result[i].length;j++)
			{
				result[i][j]=add(matrix1[i][j],matrix2[i][j]);
				
			}
			return result;
		}
		
		// multiplying two matrices;
		
		
	}
	

}
