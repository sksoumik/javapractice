package problem01;

public class Polygon {
	private int n;
	private double side;
	private double x,y;
	
	public Polygon(){
		side=1;
		n=3;
	}
	
	public Polygon(int n, double side, double x, double y){
		this.n=n;
		this.side=side;
		this.x=x;
		this.y=y;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public double getSide() {
		return side;
	}

	public void setSide(double side) {
		this.side = side;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}
	public double area(){
		
		return (n*side*side)/(4*Math.tan((n*side)/n));
	}
	public double perimeter(){
		if(side>=1)
	 side=side+1;
		return side;
		
	}
	
	
	

}

