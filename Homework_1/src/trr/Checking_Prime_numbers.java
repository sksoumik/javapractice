package trr;

public class Checking_Prime_numbers {

	public static void main(String[] args) {
           int num_of_primes=1000;
           int num_of_primes_perline=8;
           int count=0;
           int num=2;
           System.out.println("the prime numbers from 2 to 1000 are: ");
           
           while(num<=1000){
        	   boolean isPrime=true;
        	   
        	   for(int d=2;d<=num/2;d++){
        		   if(num%d==0){
        			   isPrime=false;
        			   break;
        		   }
        		   
        	   }if(isPrime){
        		   count++;
        		   if(count % num_of_primes_perline==0){
        			   System.out.println(num);
        			   
        		   }else
        			   System.out.println(num+" ");
        			   
        	   }
        	   num++;
           }
	}

}
