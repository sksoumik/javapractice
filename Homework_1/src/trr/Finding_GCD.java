package trr;

import java.util.Scanner;

public class Finding_GCD {

	public static void main(String[] args) {
		
		Scanner input=new Scanner(System.in);
		System.out.println("Enter two integer: ");
		int n1=input.nextInt();
		int n2=input.nextInt();
         
		//find the minimum
		if(n2<n1){
			int temp=n2;
			n2=n1;
			n1=temp;
		}
		for(int d=n1;true;d--){
			if(n1%d==0 && n2%d==0){
				System.out.println("the GCD of the two number is: "+d);
				break;
				
			}
		}
		
	}

}
