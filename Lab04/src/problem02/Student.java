package problem02;

public class Student {
	
	  private String id;
	  private String name;
	  private String department;
	  
	  public String id(){
		  return id;
	  }
	  public String name(){
		  return name;
	  }
	  public String department(){
		  return department;
	  }
	 public String getid(){
		 return id;
	 }
	 public String getname(){
		 return name;
	 }
	 public String getdepartment(){
		 return department;
	 }
	 public Student(String id, String name, String department){
		 this.id=id;
		 this.name=name;
		 this.department=department;
	 }

}
