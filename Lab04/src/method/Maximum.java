package method;

public class Maximum {
	
	public static int findMax (int []a)
	{
		
		int max=a[0];
		for(int i=0;i<a.length;i++)
		{
			if(a[i]>max)
			{
				max=a[i];
			}
		}
		
		return max;
	}

	public static void main(String[] args) {
		
		
		int []num={2,4,5,6,7,8};
		System.out.println(findMax(num));


	}

}
