package lab;

public class Circle {
	
		private double radius;
		
		
		public  double area(){
			return Math.PI*radius*radius;
		}
		public double circumference(){
			return 2*Math.PI*radius;
		}
		public Circle(double r){
			radius=Math.abs(r);
		
	}
		public double getRadius(){
			return radius;
		}
		public void setRadius(double r){
			radius=Math.PI*radius;
			
		}

}
