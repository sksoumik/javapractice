package problem04;

public class Book {
	
	private int id;
	private String title;
	private String author;
	
	
	
	public Book (int id, String title, String author){
		
		this.id=id;
		this.title=title;
		this.author=author;
	}
	public int getid(){
		return this.id;
	}
	public String gettitle(){
		return this.title;
	}
	public String getauthor(){
		return this.author;
	}
	public void setid(){
		this.id=id;
	}
	public void settitle(){
		this.title=title;
	}
	public void setauthor(){
		this.author=author;
	}
	

}
