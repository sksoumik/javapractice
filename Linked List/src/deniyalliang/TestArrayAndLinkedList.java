package deniyalliang;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.ListIterator;

public class TestArrayAndLinkedList {

	public static void main(String[] args) {
		ArrayList<Object> arraylist=new ArrayList<>();
		arraylist.add(12);
		arraylist.add(13);
		arraylist.add(14);
		arraylist.add(15);
		arraylist.add("soumik");
		
		System.out.println("The elements in the arraylist: ");
		System.out.println(arraylist); 
		
		arraylist.add(0, 10);
		System.out.println("now the elements in the arraylist: ");
		System.out.println(arraylist);
		
		LinkedList<Object> linkedlist=new LinkedList<>(arraylist);
		linkedlist.add(2, "third");
		
		System.out.println("Printing the linked list elements::::: ");
		
		ListIterator<Object> listiterator=linkedlist.listIterator(); 
		while(listiterator.hasNext())
		{
			System.out.println(listiterator.next()); 
		}
		System.out.println();
		
		// printing the list backward
		
		System.out.println("the list in backword:::::");
		listiterator=linkedlist.listIterator(linkedlist.size());
		while(listiterator.hasPrevious())
		{
			System.out.println(listiterator.previous());
		}
		
		
		

	}

}
