package newboston;

// common string methods --- the way to find a string starting or ending with the same character 

public class CommonStringMethods {

	public static void main(String[] args) {
		String [] words={"Salman","Sadman","Sabbir","Ranbir"};
		
		for(String w: words){
			if(w.startsWith("Sa"))
				System.out.println(w + " starts with Sa"); // it will print the names which starts with #Sa
			
		}
		
		System.out.println();         //new line  (its not mandatory)  
		
		
		for(String w: words){
			if(w.endsWith("ir"))
				System.out.println( w+ " ends with ir");       // it will print the names which ends with #ir 
		}
		
		

	}

}
