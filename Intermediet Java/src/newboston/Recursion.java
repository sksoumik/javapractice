package newboston;

public class Recursion {

	public static void main(String[] args) {
		
		 System.out.println(fact(5));

	}
	
	public static int fact(int n){
		if(n<=1)                           //  it can't be zero because then the result will also become zero
			return 1;
		else
			return n*fact(n-1);    /*i need to call the function itself then it will recurse again and again
		*/
	}

}
