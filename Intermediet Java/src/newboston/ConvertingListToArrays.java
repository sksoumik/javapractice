package newboston;
import java.util.*;

public class ConvertingListToArrays {

	public static void main(String[] args) {
		
	     String [] name= {"sadman", "kabir","soumik","Zerin"};
	     LinkedList<String> thenames=new LinkedList<String>(Arrays.asList(name));
	     thenames.add("sourav");
	     thenames.addFirst("zebin");
	     
	     name=thenames.toArray(new String[thenames.size()]);
	     for(String x: name)
	    	 System.out.printf("%s ", x);


	}

}
