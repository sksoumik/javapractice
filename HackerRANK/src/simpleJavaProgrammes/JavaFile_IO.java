package simpleJavaProgrammes;
import java.io.*;
import java.util.Scanner;

public class JavaFile_IO {

	public static void main(String[] args) {
		File file = new File("text.txt");
		
		try{
			PrintWriter output = new PrintWriter(file);
			output.println("Sadman Kabir Soumik");
			output.println("Mtittika Chawdhury"); 
			output.println("Tanisha Kabir");
			output.close();
			
		}catch(IOException ex)
		{
			System.out.printf("Error %s\n",ex);
		}
		
		/*	Scanner sc = new Scanner(file);
			String name1 = sc.nextLine();
			String name2 = sc.nextLine();
			String name3 = sc.nextLine();

			System.out.printf("1 %s\n", name1);
			System.out.printf("2 %s\n", name2);

			System.out.printf("3 %s\n", name3);*/
		try {
			Scanner scan = new Scanner(file);
			int i = 0;
			while (scan.hasNext()) {
				i++;
				System.out.println(i + " " + scan.nextLine()); 
			}
		} catch (Exception e) {

			e.printStackTrace();
		}
		

	}

}
