package simpleJavaProgrammes;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Scanner;

public class Currency_formatter {

	public static void main(String[] args) {
		
		System.out.println("Enter an money amount: ");
		Scanner scanner = new Scanner(System.in);
		double payment = scanner.nextDouble();
		scanner.close();

		NumberFormat localPayment; 
        localPayment = NumberFormat.getCurrencyInstance(Locale.US);
        System.out.println("US: " + localPayment.format(payment));   
        
        
        localPayment = NumberFormat.getCurrencyInstance(new Locale("en", "in"));
        System.out.println("India: " + localPayment.format(payment));
        
        
        localPayment = NumberFormat.getCurrencyInstance(Locale.CHINA);
        System.out.println("China: " + localPayment.format(payment));
        
        
        localPayment = NumberFormat.getCurrencyInstance(Locale.FRANCE);
        System.out.println("France: " + localPayment.format(payment));
        
        
        localPayment = NumberFormat.getCurrencyInstance(Locale.CANADA);
        System.out.println("Canada: "+localPayment.format(payment));
        
        localPayment = NumberFormat.getCurrencyInstance(new Locale("en", "bd"));
        System.out.println("Bangladesh: " + localPayment.format(payment));
        
        
        
	}

}
