package simpleJavaProgrammes;
import java.util.*;

public class Java_Loop_2 {
	
	/*
	 * Problem 
	 * https://www.hackerrank.com/challenges/java-loops 
	 * 
	 * */

	public static void main(String[] args) {
		
	     Scanner in = new Scanner(System.in);
	     System.out.println("Enter the number of quaries: ");
	        int t=in.nextInt();
		     System.out.println("Enter the value of a,b and n: ");

	        for(int i=0;i<t;i++){
	            int a = in.nextInt();
	            int b = in.nextInt();
	            int n = in.nextInt();
	            for(int  j=0; j<n;j++)
	                {
	                a = a + (int)Math.pow(2, j)*b; 
	                System.out.print(a + " ");
	            }
	            System.out.println("");
	        }
	        in.close();

	}

}
