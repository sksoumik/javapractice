package simpleJavaProgrammes;
import java.lang.*;
import java.util.Scanner;
import java.util.*;

/*
 * 
 * problem
 * https://www.hackerrank.com/challenges/java-static-initializer-block
 * */
public class Static_initializer {

	static int B, H;
	static boolean flag;

	static {
		System.out.println("Enter the value of Breadth and height: "); 
		Scanner input = new Scanner(System.in);
		B = input.nextInt();
		H = input.nextInt();
		if (B > 0 && H > 0) {
			flag = true;
		} else {
			System.out.println("java.lang.Exception: Breadth and height must be positive"); 
		}
	}

	public static void main(String[] args) {
		if (flag) {
			int area = B * H;
			System.out.println(area); 
		}
	}
}

