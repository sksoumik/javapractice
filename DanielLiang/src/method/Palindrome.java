package method;

/*
 * (Palindrome integer) Write the following two methods
// Return the reversal of an integer, i.e. reverse(456) returns 654
public static int reverse(int number)
// Return true if number is palindrome
public static boolean isPalindrome(int number)
Use the reverse method to implement isPalindrome. A number is a palindrome
if its reversal is the same as itself. Write a test program that prompts the
user to enter an integer and reports whether the integer is a palindrome.
*/

public class Palindrome {
	
	
	public static int reverse (int num)
	{
		
		int rem=0;
		while(num>0)
		{
			rem=(num%10)+(rem*10);
			num=num/10;
			
			
		}
		return rem;
	}
	
	public static boolean isPalindrome(int num)
	{
		if(num==reverse(num))
		return true;
		else
			return false;
	}

	public static void main(String[] args) {
		System.out.println(isPalindrome(123));

	}

}
