package method;

import java.util.Scanner;

public class Patern_5point6 {
	
	
	
	public static void displayPattern(int n)
	{
		for(int i=1;i<=n;i++)
		{
			for(int j=n;j>=i;j--)
			{
			
				System.out.print(j+" ");
			}
				
			
			System.out.println();
			
		}
		
	}

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		System.out.println("Enter the value of number: ");
		int num=input.nextInt();
		displayPattern(num);
		
		

	}

}
