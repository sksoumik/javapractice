package method;

import java.util.Scanner;

public class Deviation {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		double[]numbers=new double[5];
		System.out.println("Enter the any numbers: ");
		
		for(int i=0;i<numbers.length;i++)
		{
			numbers[i]=input.nextDouble();
		}
		System.out.println("The mean number is : "+mean(numbers));
		
		System.out.println("The deviation  number is : "+deviation(numbers));


	}
	
	public static double mean(double[]x)
	{
		double sum=0;
		for(int i=0;i<x.length;i++)
		{
			sum=sum+x[i];
		}
		return sum/x.length;
	}
	public static double deviation(double[]x)
	{
		double sumsq=0;
		double mean=mean(x);
		for(int i=0;i<x.length;i++)
		{
			sumsq=sumsq+Math.pow((x[i]-mean), 2);
		}
		return Math.sqrt(sumsq/(x.length-1));
		
		
		
	}

}
