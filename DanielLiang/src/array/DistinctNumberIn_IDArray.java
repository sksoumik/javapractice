package array;

public class DistinctNumberIn_IDArray {

	public static void main(String[] args) {


		int [] arr={2,3,4,3,3,4,2,9};
		
		
		for(int i=0;i<arr.length;i++)
		{
			boolean isDistinct=false;
			
			for(int j=0;j<i;j++)
			{
				if(arr[i]==arr[j])
			    isDistinct=true;
				break;
				
			}
			if(!isDistinct)
			{
				System.out.print(arr[i]+"  ");
				i+=1;
			}
			
		}
		
	}

}
