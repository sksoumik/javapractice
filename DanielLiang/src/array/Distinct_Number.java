package array;
/*Printing distinct numbers) Write a program that reads in ten numbers and displays
distinct numbers*/

import java.util.Scanner;

public class Distinct_Number {

	public static void main(String[] args) {
		
		//public static void main(String[] args) {
	        Scanner input=new Scanner(System.in);
	        int[] values = new int[10];
	        int amount = 0;
	        System.out.print("Enter ten numbers: ");        

	        for(int i = 0; i < 10; i++) {
	            int num = input.nextInt();
	            boolean distinct = true;
	            for(int j = 0; j < amount; j++) {
	                if (num == values[j]) {
	                    distinct = false;
	                    break;
	                }
	            }
	            if (distinct) values[amount++] = num;
	        }
	        System.out.println("The distinct numbers are: "); 
	        for(int i = 0 ; i < amount; i++) System.out.print(values[i]+" ");   
	    }


	}


