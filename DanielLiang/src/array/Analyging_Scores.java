package array;

/*
 * (Analyzing scores) Write a program that reads an unspecified number of scores
and determines how many scores are above or equal to the average and how many
scores are below the average. Enter a negative number to signify the end of the
input. Assume that the maximum number of scores is 10
*/

import java.util.Scanner;

public class Analyging_Scores {

	public static void main(String[] args) {
		Scanner input=new Scanner(System.in);
		int NUMBER_OF_MAXIMUM=10;
		int [] score=new int[NUMBER_OF_MAXIMUM];
		int sum=0,avg=0,n=0;
		System.out.println("Enter the number of scores(give the last input a negative number): ");
		for(int i=0;i<score.length;i++){
			score[i]=input.nextInt();
			
			if(score[i]<0)
			{
				avg=sum/i;
				break;
			}
			else
			{
				sum+=score[i];
				n++;
			}
			
			
		}
		int equal=0, below=0, above=0;
		for(int i=0;i<n;i++){
			if(score[i]==avg){
				equal++;
			}
			else if(score[i]>avg){
				above++;
			}
			else{
				below++;
			}
		}
		System.out.println("The number of scores equal to the average "+equal);
		System.out.println("The number of scores above to the average "+above);
		System.out.println("The number of below equal to the average "+below);

		

	}

}
