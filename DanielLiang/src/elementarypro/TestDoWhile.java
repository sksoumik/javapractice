package elementarypro;
import java.util.Scanner;
public class TestDoWhile {

	public static void main(String[] args) {
		Scanner input= new Scanner(System.in);
		int data;
		int sum=0;
		do{
			
			System.out.println("Enter an integer: ");
			data=input.nextInt();
			sum+=data;
			
		}while(data!=0);
		System.out.println("The sum is: "+sum);
			

	}

}
