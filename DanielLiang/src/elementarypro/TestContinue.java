package elementarypro;

import java.util.Scanner;

public class TestContinue {

	public static void main(String[] args) {
     
		Scanner input=new Scanner(System.in);
		System.out.println("Enter the value for a number:");
		int num=input.nextInt();
		int  sum=0;
		
		while(num<20){
			num++;
			if(num==10 || num==11)
				continue;
			sum+=num;
			
		}
		System.out.println("The sum is: "+sum);
		

	}

}
