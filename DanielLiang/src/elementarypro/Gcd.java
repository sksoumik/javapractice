package elementarypro;

import java.util.Scanner;

public class Gcd {

	public static void main(String[] args) {
		
      Scanner input=new Scanner(System.in);
      System.out.println("Enter an integer value: ");
      int n1=input.nextInt();
      
      System.out.println("Enter an integer value: ");
      int n2=input.nextInt();
      int gcd=1;  //initial GCD for 1
      int k=2;//possible GCD
      while(k<=n1 && k<=n2){
    	  if(n1%k==0 && n2%k==0)
    		  gcd=k;//update GCD
    	  k++;
      }
      System.out.println("The greatest commmon divisor for"  +n1+ " and "+n2+" is "+gcd);


	}

}
