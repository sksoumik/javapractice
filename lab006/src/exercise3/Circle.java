package exercise3;

public class Circle 
{
	private double radious;
	
	public Circle()
	{
		
	}
	public Circle(double radious)
	{
		this.radious=radious;
	}
	public double getRadious()
	{
		return radious;
	}
	public void setRadious(double radious)
	{
		this.radious=radious;
	}
	public double area()
	{
		return Math.PI*radious*radious;
	}
	public double perimeter()
	{
		return 2*Math.PI*radious;
	}
	
}







