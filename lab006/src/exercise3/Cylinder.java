package exercise3;

public class Cylinder extends Circle
{
	private double height;
	
	public Cylinder()
	{
		
	}
	public Cylinder(double height)
	{
		this.height=height;
	}
	public Cylinder (double radious,double height)
	{
		super(radious);
		this.height=height;
	}
	public double getHeight()
	{
		return height;
	}
	public void setHeight(double height)
	{
		this.height=height;
	}
	public double getvolume()
	{
		return super.area() * height;
	}
}
