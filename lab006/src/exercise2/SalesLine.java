package exercise2;

public class SalesLine 
{
	private Product product;
	private double quantity;
	private double subTotal;
	
	public SalesLine(Product product,double quantity)
	{
		this.product=product;
		this.quantity=quantity;
		subTotal=product.getPrice()*quantity;
	}
	public Product getProduct()
	{
		return product;
	}
	public double getQuantity()
	{
		return quantity;
	}
	public double getSubTotal()
	{
		return subTotal;
	}
	public void setProduct(Product product)
	{
		this.product=product;
	}
	public void setQuantity(double quantity)
	{
		this.quantity=quantity;
	}
	public void setSubTotal(double subTotal)
	{	

		this.subTotal=subTotal;
	}
}








