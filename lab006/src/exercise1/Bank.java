package exercise1;

public class Bank
{
	private Account[] accounts;
	
	public Bank(int size)
	{
		accounts = new Account[size];
	}
	public Account getAccount (String number)
	{
		for(int i=0;i<accounts.length;i++)
		{
			if(accounts[i].getNumber().equals(number)==true)
			{
				return accounts[i];
			}	
		}
		return null;
	}
	public void addAccount (Account account,int index)
	{	
		
		accounts[index] = account;
	}
}
