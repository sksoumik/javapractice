package exercise1;

public class main 
{

	public static void main(String[] args) 
	{
		Bank b = new Bank(2);
		
		Account a1=new Account("151","fasial",5000.5);
		Account a2=new Account("141","sakib",5555.5);
		
		b.addAccount(a1,0);
		b.addAccount(a2,1);
		
		Account a3 = b.getAccount(a1.getNumber());
		
		System.out.print(a3.getNumber()+","+a3.getName()+","+a3.getbalance());
		
	}

}
