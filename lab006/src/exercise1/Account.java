package exercise1;

public class Account
{
	private String number;
	private String name;
	private double balance;
	
	public Account (String number,String name , double balance)
	{
		this.number=number;
		this.name=name;
		this.balance=balance;
	}
	
	public String getNumber()
	{
		return number;
	}
	public String getName()
	{
		return name;
	}
	public double getbalance()
	{
		return balance;
	}
	public void setNumber(String number)
	{
		this.number=number;
	}
	public void setName(String name)
	{
		this.name=name;
	}
	public void setBalance(double balance)
	{
		this.balance=balance;
	}
}











