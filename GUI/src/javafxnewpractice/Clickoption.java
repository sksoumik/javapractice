package javafxnewpractice;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;




public class Clickoption extends Application {
	public static void main(String [] args){
		launch(args);
	}
	@Override
	public void start(Stage primaryStage) throws Exception {
		Button click=new Button("click here");
		Button exit=new Button("exit");
		exit.setOnAction(e -> {
		
			System.out.println("you have exited");
			System.exit(0);
		});

		
		click.setOnAction(new EventHandler<ActionEvent>() {
			
			@Override
			public void handle(ActionEvent event) {
           System.out.println("You have started GUI in java");				
			}
		});
		
		VBox root=new VBox();
		root.getChildren().addAll(click,exit);
		Scene scene=new Scene(root,700,500);
		primaryStage.setTitle("WIndows");
		primaryStage.setScene(scene);
		primaryStage.show();
		
		
		
		
	}
	

}
