package calculatorMaking;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class Calculator implements ActionListener{
	private JFrame window;
	private JTextField inp1;
	private JTextField inp2;
	private JTextField result;
	private JButton addition;
	private JButton subtraction;
	private JButton multiplication;
	private JButton division;
	
	 public Calculator(){
		 
		 window=new JFrame();
		 window.setSize(300, 250);
		 window.setLayout(new GridLayout(8,2));
		 
		 inp1=new JTextField("Input1");
		 inp2=new JTextField("Input2");
		 
		 result=new JTextField("Result");
		 
		 addition=new JButton("+");
		 addition.addActionListener(this);
		 

		 subtraction=new JButton("-");
		 subtraction.addActionListener(this);
		 
		 multiplication=new JButton("*");
		 multiplication.addActionListener(this);
		 
		 division=new JButton("/");
		 division.addActionListener(this);
		 
		 
		 window.add(new JLabel());
		 window.add(inp1);
		 window.add(inp2);
		 window.add(result);
		 window.add(addition);
		 window.add(subtraction);		
		 window.add(multiplication);
		 window.add(division);
		 window.setVisible(true);
		 window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		 



		 
	 }

	
	
	
	
	
	
	
	
	

	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource()==addition){
			
			double i1=Double.parseDouble(inp1.getText());
			double i2 =Double.parseDouble(inp2.getText());
			double r=i1+i2;
			result.setText(r+" ");
			
			
					
		}
		else if(e.getSource()==subtraction){
			
			double i1=Double.parseDouble(inp1.getText());
			double i2 =Double.parseDouble(inp2.getText());
			double r=i1-i2;
			result.setText(r+" ");
			
			
					
		}if(e.getSource()==multiplication){
			
			double i1=Double.parseDouble(inp1.getText());
			double i2 =Double.parseDouble(inp2.getText());
			double r=i1*i2;
			result.setText(r+" ");
			
			
					
		}if(e.getSource()==division){
			
			double i1=Double.parseDouble(inp1.getText());
			double i2 =Double.parseDouble(inp2.getText());
			double r=i1/i2;
			result.setText(r+" ");
			
			
					
		}
		
	}
	
	
	public static void main(String[]args){
		new Calculator();
	}
	
	

}
