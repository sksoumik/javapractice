package bubbleSort;

public class Bubble {
	
	private long a[];
	private int n;
	
	public Bubble(int max)
	{
		a = new long[max];
		n = 0;
	}
	
	public void insert(long value)
	{
		a[n]= value;
		n++;
		
	}
	public void display()
	{
		for(int i=0; i<n;i++)
		{
			System.out.print(a[i]+" ");
		}
	}
	public void bubblesort()
	{
		int out, in;
		for(out = n-1; out>1; out++)
			for(in = 0; in<n; in++)
				if(a[in]>a[in+1])
					swap(in, in+1);
	}
	public void swap(int one, int two)
	{
		long temp = a[one];
		a[one]=a[two];
		a[two]=temp;
	}

}
