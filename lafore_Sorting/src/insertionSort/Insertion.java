package insertionSort;

import java.util.Scanner;

public class Insertion {
	
	
	public static int [] InsertionSort( int [] list)
	{
		int i,j,key,temp ; 
		for(i=0; i< list.length;i++)
		{
			key = list[i];
			j = i-1;
			while(j >= 0 && key < list[j])
			{
				temp = list[j];
				list[j]= list[j+1];
				list[j+1]= temp;
				j--;
			}
		}
		return list;
	} 
	public static void main(String[] args)
	{
		
		/************Creating an array *************/
		 int [] arr = new int [10];
		 /************Enter few itens in the array*************/
		 System.out.println("Enter few items in the array");
		 Scanner input =new Scanner(System.in);
		 
		 for(int k=0; k<arr.length; k++)
		 {
			 arr[k] = input.nextInt();
		 }
		 
		 /************Display after Insertion sort *************/
		 System.out.println("\nThe items after insertion sort: \n");
		 
		 for(int i = 0 ; i< arr.length;i++)
		 {
			 InsertionSort(arr);
			 System.out.print(arr[i]+" "); 
			 
		 }
		 
		
	}
	
	
	
	
}
