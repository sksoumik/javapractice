package selectionSort;

public class SelectionMain {

	public static void main(String[] args) {
		
		int max =10;
		Selection ar;
		
		ar= new Selection(max);
		
		ar.insert(10);
		ar.insert(9);
        ar.insert(12);
        ar.insert(15);
        ar.insert(5);
        ar.insert(8);
		ar.insert(19);
		ar.insert(16);
		ar.display();
		ar.SelectionSort();
		System.out.println("\nAfter selection Sort");
		ar.display();


	}

}
