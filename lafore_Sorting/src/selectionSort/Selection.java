package selectionSort;

public class Selection {
	private int a[];
	private int n;
	
	public Selection(int max)
	{
		a = new int [max];
		n = 0;
	}
	public void insert(int value)
	{
		a[n] = value;
		n++;
		
	}
	public void display()
	{
		for(int i = 0; i < n;i++)
		{
			System.out.print(a[i]+" ");
		}
	}
	public void swap(int one, int two)
	{
		int temp = a[one];
		a[one]=a[two];
		a[two]=temp;
	}
	
	public void SelectionSort()
	{
		int in,out, min; // in = inner loop ; out = outer loop
		for(out = 0; out < n-1 ; out++)
		{
			min =   out; //at first a[0] min, then when loops run the min becomes a[1] .... 

			for(in = out+1;in<n;in++)     // comparing with the 
			{
				if(a[in]<a[min])
					min = in;
					swap(out,min);
			}
			
		}
		
		
	}
	

}
