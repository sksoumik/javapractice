package objectSort;

public class ArrayInOb {
	private Person[] a; 
	private int nElems; 
	//--------------------------------------------------------------
	
	public ArrayInOb(int max) 
	{
        a = new Person[max]; 
	    nElems = 0; 
	}
	
	//--------------------------------------------------------------
	
	public void insert(String last, String first, int age)
	{
	a[nElems] = new Person(last, first, age);
	nElems++; 
	}
	//--------------------------------------------------------------
	
	public void display() 
	{
	for(int j=0; j<nElems; j++) 
	a[j].displayPerson(); 
	System.out.println("");
	}
	
	//------------------------insertion sort by last name--------------------------------------
	
	public void insertionSortByLastName()
	
//-------------------insertion sort by AGE---------------------------------------------------
	{
	int  i, j;           // i for outer loop and j for inner loop 
	for(i=1; i<nElems; i++) 
	{
	Person temp = a[i];    
	j = i; 
	while(j>0 && a[j-1].getLast().compareTo(temp.getLast())>0)
	{
	  a[j] = a[j-1]; // shift item to the right
	  --j;  // go left one position
	}
	
	   a[j] = temp; // insert marked item
	} 
	} 
	public void insertionSortByAge()
	{
	int j, i;  
	for(i=1; i<nElems; i++) 
	{
	Person temp = a[i];  
	j = i; 
	while(j>0 && Integer.compare(a[j-1].getAge(), temp.getAge())>0)
	{
	  a[j] = a[j-1]; // shift item to the right
	  --j;  // go left one position
	}
	
	   a[j] = temp; // insert marked item 
	} 
	} 
	
	}
