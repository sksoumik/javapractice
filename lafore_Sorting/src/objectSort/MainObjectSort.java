package objectSort;

public class MainObjectSort {

	public static void main(String[] args) {
		
		int maxsize =100;
		
		ArrayInOb ar;
		ar = new ArrayInOb(maxsize);
		ar.insert("Soumik", "Sadman", 30);
		ar.insert("Sujit", "Debnath", 24);
		ar.insert("Azwad", "Chawdhury", 21);
		ar.insert("Joydip", "Chawdhury", 20);
		ar.insert("Niyaz", "Niloy", 23);
		ar.insert("Limon", "Hashem", 20);
        System.out.println("Before Insertion Sort: \n");

		ar.display();
         ar.insertionSortByLastName();
		System.out.println("\n After insertion sort \n");
		ar.display();
		
		ar.insertionSortByAge();
		System.out.println("\n After insertion sort by age: \n");
		ar.display();

		
		

		
		

	}

}
