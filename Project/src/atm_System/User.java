package atm_System;
import java.util.ArrayList;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class User {
	
	private String firstName;
	private String lastName;
	private String uuid;
	private byte pinHash[];
	private ArrayList<Account> accounts;
	public User(String firstName, String  lastName, String pin, Bank theBank ){
		
		this.firstName=firstName;
		this.lastName=lastName;
		
		/*store the pin's md5 value rather than the original value for security purpose*/
		
		try {
			MessageDigest md=MessageDigest.getInstance("MD5");
			
			this.pinHash=md.digest(pin.getBytes());
			
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Error. there is no such a method");
			e.printStackTrace();
			System.exit(1);
		}
		this.uuid=theBank.getNewUserUUID();
		
		
	}



}
